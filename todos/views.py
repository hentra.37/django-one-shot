from ast import keyword
from django.shortcuts import redirect
from django.shortcuts import render
import pkg_resources
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolist"


class TodoListDetail(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo"


class TodoUpdate(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoCreate(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoDelete(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


class TodoItemCreate(CreateView):
    model = TodoItem
    template_name = "todos/item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args={self.object.list.pk})

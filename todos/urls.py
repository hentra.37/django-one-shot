from django.urls import path
from todos.views import (
    TodoListView,
    TodoListDetail,
    TodoCreate,
    TodoUpdate,
    TodoDelete,
    TodoItemCreate,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoListDetail.as_view(), name="show_todolist"),
    path("create/", TodoCreate.as_view(), name="create_todolist"),
    path("<int:pk>/edit/", TodoUpdate.as_view(), name="update_todolist"),
    path("<int:pk>/delete/", TodoDelete.as_view(), name="delete_todolist"),
    path("items/create/", TodoItemCreate.as_view(), name="create_todoitem"),
]
